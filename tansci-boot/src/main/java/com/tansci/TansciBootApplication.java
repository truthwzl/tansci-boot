package com.tansci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TansciBootApplication {

    /**
     * 主函数，程序的入口点。
     *
     * @param args 包含命令行参数的字符串数组
     */
    public static void main(String[] args) {
        SpringApplication.run(TansciBootApplication.class, args);
    }

}
